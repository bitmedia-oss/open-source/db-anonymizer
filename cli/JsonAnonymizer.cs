﻿using Newtonsoft.Json.Linq;
using System;
using System.Security.Cryptography;
using System.Text;

namespace dbanonymizer
{
    class JsonAnonymizer
    {
        private readonly JobYaml job;
        private readonly ConfigYaml config;
        internal JsonAnonymizer(JobYaml job, ConfigYaml config)
        {
            this.job = job;
            this.config = config;
        }

        public void process(JToken jRoot, string ctx)
        {
            foreach (var p in config.Json.ObfuscateJson)
            {
                var when = jRoot.SelectToken(p.When);
                if (when == null)
                {
                    continue;
                }
                if (!string.IsNullOrEmpty(p.Is))
                {
                    var val = when.Value<string>();
                    if (string.IsNullOrEmpty(val))
                    {
                        continue;
                    }
                    if (!string.Equals(p.Is, val))
                    {
                        continue;
                    }
                }
                foreach (var then in p.Then)
                {
                    foreach (var token in jRoot.SelectTokens(then.Key))
                    {
                        if (token.Type != JTokenType.String)
                        {
                            Console.WriteLine($"WARN {ctx}: {token.Path} is not a string");
                            continue;
                        }

                        token.Replace(obfuscate(job, token.Value<string>(), then.Value));
                    }
                }
            }

        }
        public bool Replace(JobYaml job, string value, out string newValue)
        {
            if (string.IsNullOrEmpty(value))
            {
                newValue = value;
                return true;
            }
            foreach (var except in job.Except)
            {
                if (string.Equals(except, value))
                {
                    newValue = value;
                    return true;
                }
            }
            if (job.Replace.TryGetValue(value, out newValue))
            {
                return true;
            }
            newValue = null;
            return false;
        }
        private string md5(string value)
        {
            using (MD5 md5Hash = MD5.Create())
            {
                byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(value));

                // Create a new Stringbuilder to collect the bytes
                // and create a string.
                StringBuilder sBuilder = new StringBuilder();

                // Loop through each byte of the hashed data 
                // and format each one as a hexadecimal string.
                for (int i = 0; i < data.Length; i++)
                {
                    sBuilder.Append(data[i].ToString("x2"));
                }

                // Return the hexadecimal string.
                return sBuilder.ToString();
            }
        }

        private string obfuscate(JobYaml job, string v, ConfigYaml.ObfuscationMethodEnum value)
        {
            switch (value)
            {
                case ConfigYaml.ObfuscationMethodEnum.Keep:
                    return v;
                case ConfigYaml.ObfuscationMethodEnum.Replace:
                    {
                        if (Replace(job, v, out var newValue))
                        {
                            return newValue;
                        }
                        return v;
                    }
                case ConfigYaml.ObfuscationMethodEnum.Clear:
                    {
                        if (Replace(job, v, out var newValue))
                        {
                            return newValue;
                        }
                        return null;
                    }
                case ConfigYaml.ObfuscationMethodEnum.MD5:
                    {
                        if (Replace(job, v, out var newValue))
                        {
                            return newValue;
                        }
                        return "ANON_" + md5(v);
                    }
                case ConfigYaml.ObfuscationMethodEnum.Salted:
                    {
                        if (Replace(job, v, out var newValue))
                        {
                            return newValue;
                        }
                        return "ANON_" + md5(job.Salt + v);
                    }
                case ConfigYaml.ObfuscationMethodEnum.Short:
                    {
                        if (Replace(job, v, out var newValue))
                        {
                            return newValue;
                        }
                        return md5(job.Salt + v).Substring(0, 4);
                    }

                case ConfigYaml.ObfuscationMethodEnum.EMail:
                    {
                        if (Replace(job, v, out var newValue))
                        {
                            return newValue;
                        }
                        return "ANON_" + md5(job.Salt + v) + '@' + job.EMailDomain;
                    }
                case ConfigYaml.ObfuscationMethodEnum.Birthdate:
                    {
                        return "2001-01-01";
                    }
                default:
                    Console.WriteLine($"ERROR: JSON-Obfuscation Method '{value}' not implemented");
                    return string.Empty;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace dbanonymizer
{
    internal class ConfigFiles
    {
        private readonly Dictionary<string, ConfigYaml> configs = new Dictionary<string, ConfigYaml>();
        private readonly IDeserializer deserializer = new DeserializerBuilder().WithNamingConvention(new CamelCaseNamingConvention()).Build();

        internal void ReadDirectory(DirectoryInfo dir)
        {
            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(dir.FullName);
            }
            var files = dir.GetFiles("*.yml", SearchOption.AllDirectories);
            foreach (var f in files)
            {
                ReadFile(f);
            }

        }
        internal void ReadFile(FileInfo file)
        {
            var config = Program.ReadYAML<ConfigYaml>(file);
            if (config == null)
            {
                return;
            }
            if (configs.ContainsKey(config.Id))
            {
                Console.WriteLine($"WARN: {file.FullName} overides {config.Id}");
            }
            configs[config.Id] = config;
        }

        internal ConfigYaml Get(string config)
        {
            configs.TryGetValue(config, out var retVal);
            return retVal;
        }
    }
}

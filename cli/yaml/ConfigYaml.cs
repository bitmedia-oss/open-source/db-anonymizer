﻿using System.Collections.Generic;

namespace dbanonymizer
{
    public class ConfigYaml
    {
        public string Id { get; set; }
        public string DefaultEndpoint { get; set; }
        public string DefaultUsername { get; set; }
        public string DefaultPassword { get; set; }
        public enum TypeEnum
        {
            Postgres,
            MSSQL,
            CouchDB
        }
        public enum ObfuscationMethodEnum
        {
            MD5,
            Short,
            EMail,
            Salted,
            Birthdate,
            Replace,
            Clear,
            Keep
        }
        public string DefaultSchema { get; set; }
        public string DefaultDatabase { get; set; }

        public TypeEnum Type { get; set; }

        public enum OutputFileNameEnum
        {
            NoSplit,
            SplitOnFirstComma,
            SplitOnEveryComma
        }

        public class _ObfuscateJson
        {
            public string When { get; set; }
            public string Is { get; set; }
            public Dictionary<string, ObfuscationMethodEnum> Then { get; set; } = new Dictionary<string, ObfuscationMethodEnum>();
        }

        public Dictionary<string, Dictionary<string, ObfuscationMethodEnum>> Obfuscate { get; set; } = new Dictionary<string, Dictionary<string, ObfuscationMethodEnum>>();

        public class _Json
        {
            public List<string> ExceptDocId { get; set; } = new List<string>();
            public OutputFileNameEnum OutputFileNameHandling { get; set; } = OutputFileNameEnum.SplitOnEveryComma;
            public bool NoPrettyPrint { get; set; }
            public bool NoAttachments { get; set; }
            public List<_ObfuscateJson> ObfuscateJson { get; set; } = new List<_ObfuscateJson>();

        }
        public _Json Json { get; set; } = new _Json();
    }
}

﻿using System.Collections.Generic;

namespace dbanonymizer
{
    public class JobYaml
    {
        public string Salt { get; set; } = string.Empty;
        public string EMailDomain { get; set; } = "example.org";

        public List<string> Except { get; set; }
        public Dictionary<string, string> Replace { get; set; }
        public class _Anonymize
        {
            public string Config { get; set; }
            public string Pod { get; set; }

            public string Schema { get; set; }
            public string Endpoint { get; set; }
            public string Username { get; set; }
            public string Password { get; set; }
            public string Database { get; set; }
            public bool Disabled { get; set; }
        }
        public string Namespace { get; set; }
        public Dictionary<string, _Anonymize> Anonymize { get; set; } = new Dictionary<string, _Anonymize>();


    }
}

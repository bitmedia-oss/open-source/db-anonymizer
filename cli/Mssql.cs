﻿using System.Linq;

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace dbanonymizer
{
    class MSSQL : IHandler
    {
        private void except_and_replace(JobYaml job, StringBuilder sqlFile)
        {
            sqlFile.AppendLine($"   WHEN '' THEN ''");
            if (job.Except != null)
            {
                foreach (var name in job.Except)
                {
                    sqlFile.AppendLine($"   WHEN '{name}' THEN '{name}'");
                }
            }
            if (job.Replace != null)
            {
                foreach (var kv in job.Replace)
                {
                    sqlFile.AppendLine($"   WHEN '{kv.Key}' THEN '{kv.Value}'");
                }
            }
        }

        private void obfuscate_md5(JobYaml job, StringBuilder sqlFile, string schema)
        {
            sqlFile.AppendLine($"CREATE OR ALTER FUNCTION [{schema}].obfuscate_md5");
            sqlFile.AppendLine("(");
            sqlFile.AppendLine("  @raw nvarchar(255)");
            sqlFile.AppendLine(")");
            sqlFile.AppendLine("RETURNS varchar(37)");
            sqlFile.AppendLine("AS");
            sqlFile.AppendLine("BEGIN");
            sqlFile.AppendLine("  RETURN CASE @raw");
            except_and_replace(job, sqlFile);
            sqlFile.AppendLine($"     ELSE CONCAT('ANON_',LOWER(CONVERT(VARCHAR(32), HashBytes('MD5', CONVERT(varchar, @raw)), 2)))");
            sqlFile.AppendLine("  END;");
            sqlFile.AppendLine("END");
            sqlFile.AppendLine("GO");
        }

        private void obfuscate_short(JobYaml job, StringBuilder sqlFile, string schema)
        {
            sqlFile.AppendLine($"CREATE OR ALTER FUNCTION [{schema}].obfuscate_short");
            sqlFile.AppendLine("(");
            sqlFile.AppendLine("  @raw nvarchar(255)");
            sqlFile.AppendLine(")");
            sqlFile.AppendLine("RETURNS varchar(4)");
            sqlFile.AppendLine("AS");
            sqlFile.AppendLine("BEGIN");
            sqlFile.AppendLine("  RETURN CASE @raw");
            except_and_replace(job, sqlFile);
            sqlFile.AppendLine($"     ELSE SUBSTRING(LOWER(CONVERT(VARCHAR(32), HashBytes('MD5', CONCAT('{job.Salt}', CONVERT(varchar, @raw))), 2)),1,4)");
            sqlFile.AppendLine("  END;");
            sqlFile.AppendLine("END");
            sqlFile.AppendLine("GO");
        }



        private void obfuscate_salted(JobYaml job, StringBuilder sqlFile, string schema)
        {
            sqlFile.AppendLine($"CREATE OR ALTER FUNCTION [{schema}].obfuscate_salted");
            sqlFile.AppendLine("(");
            sqlFile.AppendLine("  @raw nvarchar(255)");
            sqlFile.AppendLine(")");
            sqlFile.AppendLine("RETURNS varchar(37)");
            sqlFile.AppendLine("AS");
            sqlFile.AppendLine("BEGIN");
            sqlFile.AppendLine("  RETURN CASE @raw");
            except_and_replace(job, sqlFile);
            sqlFile.AppendLine($"     ELSE CONCAT('ANONY_', LOWER(CONVERT(VARCHAR(32), HashBytes('MD5', CONCAT('{job.Salt}', CONVERT(varchar, @raw))), 2)))");
            sqlFile.AppendLine("  END;");
            sqlFile.AppendLine("END");
            sqlFile.AppendLine("GO");
        }


        private void obfuscate_replace(JobYaml job, StringBuilder sqlFile, string schema)
        {
            sqlFile.AppendLine($"CREATE OR ALTER FUNCTION [{schema}].obfuscate_replace");
            sqlFile.AppendLine("(");
            sqlFile.AppendLine("  @raw nvarchar(255)");
            sqlFile.AppendLine(")");
            sqlFile.AppendLine("RETURNS varchar(32)");
            sqlFile.AppendLine("AS");
            sqlFile.AppendLine("BEGIN");
            sqlFile.AppendLine("  RETURN CASE @raw");
            except_and_replace(job, sqlFile);
            sqlFile.AppendLine($"     ELSE @raw");
            sqlFile.AppendLine("  END;");
            sqlFile.AppendLine("END");
            sqlFile.AppendLine("GO");
        }


        private void obfuscate_clear(JobYaml job, StringBuilder sqlFile, string schema)
        {
            sqlFile.AppendLine($"CREATE OR ALTER FUNCTION [{schema}].obfuscate_clear");
            sqlFile.AppendLine("(");
            sqlFile.AppendLine("  @raw nvarchar(255)");
            sqlFile.AppendLine(")");
            sqlFile.AppendLine("RETURNS varchar(32)");
            sqlFile.AppendLine("AS");
            sqlFile.AppendLine("BEGIN");
            sqlFile.AppendLine("  RETURN CASE @raw");
            except_and_replace(job, sqlFile);
            sqlFile.AppendLine($"     ELSE null");
            sqlFile.AppendLine("  END;");
            sqlFile.AppendLine("END");
            sqlFile.AppendLine("GO");
        }

        private void obfuscate_email(JobYaml job, StringBuilder sqlFile, string schema)
        {
            sqlFile.AppendLine($"CREATE OR ALTER FUNCTION [{schema}].obfuscate_email");
            sqlFile.AppendLine("(");
            sqlFile.AppendLine("  @raw nvarchar(255)");
            sqlFile.AppendLine(")");
            sqlFile.AppendLine("RETURNS varchar(255)");
            sqlFile.AppendLine("AS");
            sqlFile.AppendLine("BEGIN");
            sqlFile.AppendLine("  RETURN CASE @raw");
            except_and_replace(job, sqlFile);
            sqlFile.AppendLine($"     ELSE CONCAT('ANON_', LOWER(CONVERT(VARCHAR(32), HashBytes('MD5', CONCAT('{job.Salt}', CONVERT(varchar, @raw))), 2)), '@{job.EMailDomain}')");
            sqlFile.AppendLine("  END;");
            sqlFile.AppendLine("END");
            sqlFile.AppendLine("GO");
        }

        private void obfuscate_birthdate(JobYaml job, StringBuilder sqlFile, string schema)
        {
            sqlFile.AppendLine($"CREATE OR ALTER FUNCTION [{schema}].obfuscate_birthdate");
            sqlFile.AppendLine("(");
            sqlFile.AppendLine("  @raw date");
            sqlFile.AppendLine(")");
            sqlFile.AppendLine("RETURNS date");
            sqlFile.AppendLine("AS");
            sqlFile.AppendLine("BEGIN");
            sqlFile.AppendLine($"   RETURN '2001-01-01';");
            sqlFile.AppendLine("END");
            sqlFile.AppendLine("GO");
        }

        public Task Anonymize(IOutput output, ConfigYaml config, JobYaml job, JobYaml._Anonymize anonymize)
        {
            var a = anonymize;
            var c = config;
            var database = a.Database ?? c.DefaultDatabase ?? "mssql";
            var schema = a.Schema ?? c.DefaultSchema ?? "dbo";
            var sqlFile = new System.Text.StringBuilder();

            obfuscate_md5(job, sqlFile, schema);
            obfuscate_salted(job, sqlFile, schema);
            obfuscate_email(job, sqlFile, schema);
            obfuscate_short(job, sqlFile, schema);
            obfuscate_birthdate(job, sqlFile, schema);
            obfuscate_replace(job, sqlFile, schema);
            obfuscate_clear(job, sqlFile, schema);

            foreach (var table_kv in c.Obfuscate)
            {


                var cols = table_kv.Value.Select(col_kv =>
                {
                    var col = col_kv.Key;
                    var colType = col_kv.Value;
                    switch (colType)
                    {
                        case ConfigYaml.ObfuscationMethodEnum.MD5:
                            return $"{col}=[{schema}].obfuscate_md5({col})";
                        case ConfigYaml.ObfuscationMethodEnum.Salted:
                            return $"{col}=[{schema}].obfuscate_salted({col})";
                        case ConfigYaml.ObfuscationMethodEnum.EMail:
                            return $"{col}=[{schema}].obfuscate_email({col})";
                        case ConfigYaml.ObfuscationMethodEnum.Short:
                            return $"{col}=[{schema}].obfuscate_short({col})";
                        case ConfigYaml.ObfuscationMethodEnum.Birthdate:
                            return $"{col}=[{schema}].obfuscate_birthdate({col})";
                        case ConfigYaml.ObfuscationMethodEnum.Replace:
                            return $"{col}=[{schema}].obfuscate_replace({col})";
                        case ConfigYaml.ObfuscationMethodEnum.Clear:
                            return $"{col}=[{schema}].obfuscate_clear({col})";
                        case ConfigYaml.ObfuscationMethodEnum.Keep:
                            return null;
                        default:
                            Console.WriteLine($"MSSQL does not support obfuscation '{colType}'");
                            return null;
                    }
                }).Where(x => x != null);
                var table = table_kv.Key;
                if (cols.Any())
                {
                    sqlFile.AppendLine($"UPDATE {table} SET ");
                    sqlFile.AppendLine(string.Join(",\n", cols));
                    sqlFile.AppendLine(";");
                }
            }
            sqlFile.AppendLine($"DROP FUNCTION \"{schema}\".obfuscate_md5;");
            sqlFile.AppendLine($"DROP FUNCTION \"{schema}\".obfuscate_salted;");
            sqlFile.AppendLine($"DROP FUNCTION \"{schema}\".obfuscate_email;");
            sqlFile.AppendLine($"DROP FUNCTION \"{schema}\".obfuscate_short;");
            sqlFile.AppendLine($"DROP FUNCTION \"{schema}\".obfuscate_birthdate;");
            output.WriteFile($"{a.Pod}-remote.sql", sqlFile.ToString());

            var remoteFile = new System.Text.StringBuilder();
            var tmpDir = "/tmp";
            var tmpDbName = "tmp";
            var sqlcmd = "/opt/mssql-tools/bin/sqlcmd";
            var sql = $"{sqlcmd} -S localhost -U sa -P $SA_PASSWORD";
            remoteFile.AppendLine("#!/bin/bash");
            remoteFile.AppendLine("");
            remoteFile.AppendLine("# 1. Create a new database");
            remoteFile.AppendLine($"{sql} -Q\"BACKUP DATABASE [{database}] TO DISK='{tmpDir}/orig.bak' WITH STATS=10\"");
            remoteFile.AppendLine($"{sql} -Q\"RESTORE DATABASE [{tmpDbName}] FROM DISK='{tmpDir}/orig.bak'\"");
            remoteFile.AppendLine($"{sql} -i {a.Pod}-remote.sql");
            remoteFile.AppendLine($"{sql} -Q\"BACKUP DATABASE [{tmpDbName}] TO DISK='{tmpDir}/{a.Pod}-mssqldump.bak' WITH STATS=10\"");
            remoteFile.AppendLine($"{sql} -Q\"DROP DATABASE [{tmpDbName}]\"");
            output.WriteFile($"{a.Pod}-remote.sh", remoteFile.ToString());

            return Task.CompletedTask;
        }
    }
}

﻿using System.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace dbanonymizer
{
    class Postgres : IHandler
    {
        private void except_and_replace(JobYaml job, StringBuilder sqlFile)
        {
            if (job.Except != null)
            {
                foreach (var name in job.Except)
                {
                    sqlFile.AppendLine($"   WHEN '{name}' THEN RETURN '{name}';");
                }
            }
            if (job.Replace != null)
            {
                foreach (var kv in job.Replace)
                {
                    sqlFile.AppendLine($"   WHEN '{kv.Key}' THEN RETURN '{kv.Value}';");
                }
            }
        }

        private void obfuscate_md5(JobYaml job, StringBuilder sqlFile, string schema)
        {
            sqlFile.AppendLine($"CREATE OR REPLACE FUNCTION \"{schema}\".obfuscate_md5(\"raw\" text)");
            sqlFile.AppendLine("  RETURNS text");
            sqlFile.AppendLine("  LANGUAGE 'plpgsql'");
            sqlFile.AppendLine("   VOLATILE STRICT");
            sqlFile.AppendLine("   PARALLEL SAFE");
            sqlFile.AppendLine("   COST 100");
            sqlFile.AppendLine("AS $BODY$");
            sqlFile.AppendLine("BEGIN");
            sqlFile.AppendLine("  CASE raw");
            except_and_replace(job, sqlFile);
            sqlFile.AppendLine("   ELSE RETURN CONCAT('ANON_', md5(raw));");
            sqlFile.AppendLine("END CASE;");
            sqlFile.AppendLine("END");
            sqlFile.AppendLine("$BODY$;");

        }

        private void obfuscate_short(JobYaml job, StringBuilder sqlFile, string schema)
        {
            sqlFile.AppendLine($"CREATE OR REPLACE FUNCTION \"{schema}\".obfuscate_short(\"raw\" text)");
            sqlFile.AppendLine("  RETURNS text");
            sqlFile.AppendLine("  LANGUAGE 'plpgsql'");
            sqlFile.AppendLine("   VOLATILE STRICT");
            sqlFile.AppendLine("   PARALLEL SAFE");
            sqlFile.AppendLine("   COST 100");
            sqlFile.AppendLine("AS $BODY$");
            sqlFile.AppendLine("BEGIN");
            sqlFile.AppendLine("  CASE raw");
            except_and_replace(job, sqlFile);
            sqlFile.AppendLine($"   ELSE RETURN substr(concat('{job.Salt}', md5(raw)),1,4);");
            sqlFile.AppendLine("END CASE;");
            sqlFile.AppendLine("END");
            sqlFile.AppendLine("$BODY$;");

        }


        private void obfuscate_salted(JobYaml job, StringBuilder sqlFile, string schema)
        {
            sqlFile.AppendLine($"CREATE OR REPLACE FUNCTION \"{schema}\".obfuscate_salted(\"raw\" text)");
            sqlFile.AppendLine("  RETURNS text");
            sqlFile.AppendLine("  LANGUAGE 'plpgsql'");
            sqlFile.AppendLine("   VOLATILE STRICT");
            sqlFile.AppendLine("   PARALLEL SAFE");
            sqlFile.AppendLine("   COST 100");
            sqlFile.AppendLine("AS $BODY$");
            sqlFile.AppendLine("BEGIN");
            sqlFile.AppendLine("  CASE raw");
            except_and_replace(job, sqlFile);
            sqlFile.AppendLine($"   ELSE RETURN CONCAT('ANON_', md5(concat('{job.Salt}', raw)));");
            sqlFile.AppendLine("END CASE;");
            sqlFile.AppendLine("END");
            sqlFile.AppendLine("$BODY$;");

        }


        private void obfuscate_replace(JobYaml job, StringBuilder sqlFile, string schema)
        {
            sqlFile.AppendLine($"CREATE OR REPLACE FUNCTION \"{schema}\".obfuscate_replace(\"raw\" text)");
            sqlFile.AppendLine("  RETURNS text");
            sqlFile.AppendLine("  LANGUAGE 'plpgsql'");
            sqlFile.AppendLine("   VOLATILE STRICT");
            sqlFile.AppendLine("   PARALLEL SAFE");
            sqlFile.AppendLine("   COST 100");
            sqlFile.AppendLine("AS $BODY$");
            sqlFile.AppendLine("BEGIN");
            sqlFile.AppendLine("  CASE raw");
            except_and_replace(job, sqlFile);
            sqlFile.AppendLine($"   ELSE RETURN raw;");
            sqlFile.AppendLine("END CASE;");
            sqlFile.AppendLine("END");
            sqlFile.AppendLine("$BODY$;");

        }

        private void obfuscate_clear(JobYaml job, StringBuilder sqlFile, string schema)
        {
            sqlFile.AppendLine($"CREATE OR REPLACE FUNCTION \"{schema}\".obfuscate_replace(\"raw\" text)");
            sqlFile.AppendLine("  RETURNS text");
            sqlFile.AppendLine("  LANGUAGE 'plpgsql'");
            sqlFile.AppendLine("   VOLATILE STRICT");
            sqlFile.AppendLine("   PARALLEL SAFE");
            sqlFile.AppendLine("   COST 100");
            sqlFile.AppendLine("AS $BODY$");
            sqlFile.AppendLine("BEGIN");
            sqlFile.AppendLine("  CASE raw");
            except_and_replace(job, sqlFile);
            sqlFile.AppendLine($"   ELSE RETURN NULL;");
            sqlFile.AppendLine("END CASE;");
            sqlFile.AppendLine("END");
            sqlFile.AppendLine("$BODY$;");

        }

        private void obfuscate_email(JobYaml job, StringBuilder sqlFile, string schema)
        {
            sqlFile.AppendLine($"CREATE OR REPLACE FUNCTION \"{schema}\".obfuscate_email(\"raw\" text)");
            sqlFile.AppendLine("  RETURNS text");
            sqlFile.AppendLine("  LANGUAGE 'plpgsql'");
            sqlFile.AppendLine("   VOLATILE STRICT");
            sqlFile.AppendLine("   PARALLEL SAFE");
            sqlFile.AppendLine("   COST 100");
            sqlFile.AppendLine("AS $BODY$");
            sqlFile.AppendLine("BEGIN");
            sqlFile.AppendLine("  CASE raw");
            except_and_replace(job, sqlFile);
            sqlFile.AppendLine($"   ELSE RETURN CONCAT('ANON_', md5(concat('{job.Salt}', raw)), '@{job.EMailDomain}');");
            sqlFile.AppendLine("END CASE;");
            sqlFile.AppendLine("END");
            sqlFile.AppendLine("$BODY$;");

        }

        private void obfuscate_birthdate(JobYaml job, StringBuilder sqlFile, string schema)
        {
            sqlFile.AppendLine($"CREATE OR REPLACE FUNCTION \"{schema}\".obfuscate_birthdate(\"raw\" date)");
            sqlFile.AppendLine("  RETURNS date");
            sqlFile.AppendLine("  LANGUAGE 'plpgsql'");
            sqlFile.AppendLine("   VOLATILE STRICT");
            sqlFile.AppendLine("   PARALLEL SAFE");
            sqlFile.AppendLine("   COST 100");
            sqlFile.AppendLine("AS $BODY$");
            sqlFile.AppendLine("BEGIN");
            sqlFile.AppendLine($"   RETURN '2001-01-01';");
            sqlFile.AppendLine("END");
            sqlFile.AppendLine("$BODY$;");

        }

        public Task Anonymize(IOutput output, ConfigYaml config, JobYaml job, JobYaml._Anonymize anonymize)
        {
            var a = anonymize;
            var c = config;
            var database = a.Database ?? c.DefaultDatabase ?? "postgres";
            var schema = a.Schema ?? c.DefaultSchema ?? "public";
            var sqlFile = new System.Text.StringBuilder();
            sqlFile.AppendLine($"SET search_path TO \"{schema}\";");

            obfuscate_md5(job, sqlFile, schema);
            obfuscate_salted(job, sqlFile, schema);
            obfuscate_email(job, sqlFile, schema);
            obfuscate_short(job, sqlFile, schema);
            obfuscate_birthdate(job, sqlFile, schema);
            obfuscate_replace(job, sqlFile, schema);
            obfuscate_clear(job, sqlFile, schema);

            foreach (var table_kv in c.Obfuscate)
            {
                var table = table_kv.Key;
                var cols = table_kv.Value.Select(col_kv =>
                {
                    var col = col_kv.Key;
                    var colType = col_kv.Value;
                    switch (colType)
                    {
                        case ConfigYaml.ObfuscationMethodEnum.MD5:
                            return $"{col}=obfuscate_md5({col})";
                        case ConfigYaml.ObfuscationMethodEnum.Salted:
                            return $"{col}=obfuscate_salted({col})";
                        case ConfigYaml.ObfuscationMethodEnum.EMail:
                            return $"{col}=obfuscate_email({col})";
                        case ConfigYaml.ObfuscationMethodEnum.Short:
                            return $"{col}=obfuscate_short({col})";
                        case ConfigYaml.ObfuscationMethodEnum.Birthdate:
                            return $"{col}=obfuscate_birthdate({col})";
                        case ConfigYaml.ObfuscationMethodEnum.Replace:
                            return $"{col}=obfuscate_replace({col})";
                        case ConfigYaml.ObfuscationMethodEnum.Clear:
                            return $"{col}=obfuscate_clear({col})";
                        case ConfigYaml.ObfuscationMethodEnum.Keep:
                            return null;
                        default:
                            Console.WriteLine($"Postgres Adapter does not support obfuscation '{colType}'");
                            return null;
                    }
                }).Where(x => x != null);
                if (cols.Any())
                {
                    sqlFile.AppendLine($"UPDATE {table} SET ");
                    sqlFile.AppendLine(string.Join(",\n", cols));
                    sqlFile.AppendLine(";");
                }
            }
            sqlFile.AppendLine($"DROP FUNCTION \"{schema}\".obfuscate_md5;");
            sqlFile.AppendLine($"DROP FUNCTION \"{schema}\".obfuscate_salted;");
            sqlFile.AppendLine($"DROP FUNCTION \"{schema}\".obfuscate_email;");
            sqlFile.AppendLine($"DROP FUNCTION \"{schema}\".obfuscate_short;");
            sqlFile.AppendLine($"DROP FUNCTION \"{schema}\".obfuscate_birthdate;");
            output.WriteFile($"{a.Pod}-remote.sql", sqlFile.ToString());

            var remoteFile = new System.Text.StringBuilder();
            remoteFile.AppendLine("#!/bin/bash");
            remoteFile.AppendLine("");
            remoteFile.AppendLine("# 1. Create a new database");
            remoteFile.AppendLine("export PGUSER=$POSTGRES_USER");
            remoteFile.AppendLine("export PGPASSWORD=$POSTGRES_PASSWORD");
            remoteFile.AppendLine($"createdb tmp");
            remoteFile.AppendLine($"pg_dump -d {database} -Fc | pg_restore -d tmp");
            remoteFile.AppendLine($"psql -d tmp < {a.Pod}-remote.sql");
            remoteFile.AppendLine($"pg_dump -d tmp -cOx > {a.Pod}-dump.sql");
            remoteFile.AppendLine($"dropdb tmp");
            output.WriteFile($"{a.Pod}-remote.sh", remoteFile.ToString());
            return Task.CompletedTask;
        }
    }
}

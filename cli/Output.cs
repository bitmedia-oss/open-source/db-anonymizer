﻿using System.Collections.Generic;
using System.IO;

namespace dbanonymizer
{
    class Output : IOutput

    {
        internal Output(string token, string relDir)
        {
            this.TempDir = Path.Combine(Program.EnvVars.OUT_DIR, token, relDir ?? string.Empty);
        }

        public readonly string TempDir;
        private readonly HashSet<string> createdDirs = new HashSet<string>();
        public void WriteFile(string filename, string content)
        {
            var fn = Path.Combine(TempDir, filename);
            var dir = Path.GetDirectoryName(fn);
            if (!createdDirs.Contains(dir))
            {
                Directory.CreateDirectory(dir);
                createdDirs.Add(dir);
            }
            File.WriteAllText(fn, content.Replace("\r\n", "\n"));
        }
        public void WriteFile(string filename, byte[] content)
        {
            var fn = Path.Combine(TempDir, filename);
            var dir = Path.GetDirectoryName(fn);
            if (!createdDirs.Contains(dir))
            {
                Directory.CreateDirectory(dir);
                createdDirs.Add(dir);
            }
            File.WriteAllBytes(fn, content);
        }
    }

}

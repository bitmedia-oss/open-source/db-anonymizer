﻿using MyCouch.Requests;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace dbanonymizer
{

    class Couchdb : IHandler

    {
        private string GetOutputFilename(IOutput output, ConfigYaml config, JobYaml job, string docId)
        {
            string baseName = null;
            switch (config.Json.OutputFileNameHandling)
            {
                case ConfigYaml.OutputFileNameEnum.NoSplit:
                    baseName = docId;
                    break;
                case ConfigYaml.OutputFileNameEnum.SplitOnEveryComma:
                    baseName = docId.Replace(',', '/');
                    break;
                case ConfigYaml.OutputFileNameEnum.SplitOnFirstComma:
                    baseName = string.Join('/', docId.Split(',', 2));
                    break;
            }
            return baseName + ".json";
        }
        public async Task Anonymize(IOutput output, ConfigYaml config, JobYaml job, JobYaml._Anonymize anonymize)
        {
            var started = DateTime.Now;
            var uri = new UriBuilder(anonymize.Endpoint ?? config.DefaultEndpoint);
            uri.UserName = anonymize.Username ?? config.DefaultUsername;
            uri.Password = anonymize.Password ?? config.DefaultPassword;

            var outputFormatting = config.Json.NoPrettyPrint ? Newtonsoft.Json.Formatting.None : Newtonsoft.Json.Formatting.Indented;
            var db = anonymize.Database ?? config.DefaultDatabase;
            var jsonAnon = new JsonAnonymizer(job, config);
            using (var client = new MyCouch.MyCouchClient(uri.Uri.ToString(), db))
            {
                string lastDocId = null;
                int nDoc = 0;
                while (true)
                {
                    var query = new QueryViewRequest("_all_docs");
                    query.Limit = 1000;
                    query.IncludeDocs = true;
                    query.StartKeyDocId = lastDocId;
                    var response = await client.Views.QueryAsync(query);
                    if (!response.IsSuccess)
                    {
                        Console.WriteLine($"ERROR: {response.Reason}");
                        return;
                    }
                    long lastPercentWritten = 0;
                    for (var i = 0; i < response.Rows.Length; ++i)
                    {
                        nDoc++;
                        var row = response.Rows[i];
                        lastDocId = row.Id;
                        var Percent = nDoc * 100 / response.TotalRows;
                        if (Percent > lastPercentWritten)
                        {
                            Console.Write(System.CommandLine.Rendering.Ansi.Cursor.SavePosition);
                            Console.Write($"{Percent}%");
                            Console.Write(System.CommandLine.Rendering.Ansi.Cursor.RestorePosition);
                            lastPercentWritten = Percent;
                        }
                        var fn = GetOutputFilename(output, config, job, lastDocId);
                        if (fn == null)
                        {
                            continue;
                        }
                        var jRoot = JObject.Parse(row.IncludedDoc);
                        jsonAnon.process(jRoot, lastDocId);
                        output.WriteFile(fn, jRoot.ToString(outputFormatting));
                        if (!config.Json.NoAttachments)
                        {
                            var att = jRoot.SelectToken("$._attachments");
                            if (att != null && att.Type == JTokenType.Object)
                            {
                                var objAtt = (JObject)att;
                                foreach (var kv in objAtt)
                                {
                                    var attResp = await client.Attachments.GetAsync(lastDocId, kv.Key);
                                    output.WriteFile(Path.Combine(fn.Substring(0, fn.Length - 5), kv.Key), attResp.Content);
                                }
                            }
                        }
                    }

                    if (response.Rows.Length < query.Limit)
                    {
                        break;
                    }
                }
            }
            var ended = DateTime.Now;
            Console.WriteLine($"Dumping Complete. Took {(ended - started).TotalMinutes:0.00} minutes.");
        }


    }
}

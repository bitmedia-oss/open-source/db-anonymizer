﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace dbanonymizer
{
    class Converter
    {
        public readonly ConfigFiles configFiles = new ConfigFiles();
        private readonly Dictionary<ConfigYaml.TypeEnum, IHandler> handler = new Dictionary<ConfigYaml.TypeEnum, IHandler>();
        internal Converter()
        {
            handler.Add(ConfigYaml.TypeEnum.Postgres, new Postgres());
            handler.Add(ConfigYaml.TypeEnum.MSSQL, new MSSQL());
            handler.Add(ConfigYaml.TypeEnum.CouchDB, new Couchdb());
        }

        public async Task ProcessJob(JobYaml job, string token)
        {
            try
            {
                foreach (var kv in job.Anonymize)
                {
                    var a = kv.Value;
                    var id = kv.Key;
                    if (a.Disabled)
                    {
                        continue;
                    }
                    Console.WriteLine($"[{id}]");

                    a.Pod = a.Pod.Replace("$NAMESPACE", job.Namespace);
                    var c = configFiles.Get(a.Config);
                    if (c == null)
                    {
                        Console.WriteLine($"ERROR: config {a.Config} not found");
                        return;
                    }

                    if (handler.TryGetValue(c.Type, out var h))
                    {
                        var output = new Output(token, id);
                        await h.Anonymize(output, c, job, a);
                    }
                    else
                    {
                        Console.WriteLine($"NYI: {c.Type} (in {a.Config})");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"EXCEPTION: {ex.Message}");
                Console.WriteLine(ex.StackTrace);
            }
        }
    }
}

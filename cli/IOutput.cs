﻿namespace dbanonymizer
{
    interface IOutput
    {
        void WriteFile(string filename, string content);
        void WriteFile(string filename, byte[] content);
    }
}

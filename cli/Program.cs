﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace dbanonymizer
{
    abstract class EnvVarBase
    {
        private readonly System.Text.StringBuilder help = new System.Text.StringBuilder();
        protected string GetEnv(string EnvName, string Default, string Help)
        {
            var val = Environment.GetEnvironmentVariable(EnvName) ?? Default;
            help.Append($"{EnvName:10} {Help} (Default: {Default})");
            return val;
        }
    }
    class EnvVar : EnvVarBase
    {
        public readonly string DATA_DIR;
        public readonly string JOB_DIR;
        public readonly string CONFIG_DIR;
        public readonly string OUT_DIR;
        internal EnvVar()
        {
            DATA_DIR = GetEnv("ANON_DATA_DIR", "./data", "Data Directory");
            JOB_DIR = Path.Combine(DATA_DIR, GetEnv("ANON_JOB_DIR", "job", "Job Directory (relative to data directory)"));
            CONFIG_DIR = Path.Combine(DATA_DIR, GetEnv("ANON_CONFIG_DIR", "config", "Config directory (relative to data directory)"));
            OUT_DIR = Path.Combine(DATA_DIR, GetEnv("ANON_OUT_DIR", "out", "Output directory (relative to data directory)"));
        }

    }
    class Program
    {
        public static string GetUniqueKey(int size)
        {
            char[] chars =
                "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();
            byte[] data = new byte[size];
            using (RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider())
            {
                crypto.GetBytes(data);
            }
            StringBuilder result = new StringBuilder(size);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length)]);
            }
            return result.ToString();
        }

        public static readonly EnvVar EnvVars = new EnvVar();
        static int Main(string jobFile, FileInfo[] configFiles = null, DirectoryInfo[] configDirs = null, string[] only = null, string[] except = null)
        {

            if (jobFile == null)
            {
                Console.Error.WriteLine("No --job-file was specified");
                return 1;
            }
            var converter = new Converter();
            if (configDirs == null)
            {
                var configDir = new DirectoryInfo(EnvVars.CONFIG_DIR);
                if (!configDir.Exists)
                {
                    Console.WriteLine($"Unable to find config directory ({configDir.FullName})");
                }
                else
                {
                    converter.configFiles.ReadDirectory(configDir);
                }
            }
            else
            {
                foreach (var dir in configDirs)
                {
                    converter.configFiles.ReadDirectory(dir);
                }
            }
            if (configFiles != null)
            {
                Console.WriteLine("Processing config files");
                foreach (var file in configFiles)
                {
                    converter.configFiles.ReadFile(file);
                }
            }
            var jobFileInfo = new FileInfo(Path.Combine(EnvVars.JOB_DIR, jobFile));
            Console.WriteLine($"Job: {jobFileInfo.FullName}");
            var job = Program.ReadYAML<JobYaml>(jobFileInfo);
            if (job == null)
            {
                return 1;
            }
            if (only != null)
            {
                var x = new HashSet<string>(only);
                foreach (var kv in job.Anonymize)
                {
                    kv.Value.Disabled = !x.Contains(kv.Key);
                }
            }
            if (except != null)
            {
                var x = new HashSet<string>(except);
                foreach (var kv in job.Anonymize)
                {
                    if (x.Contains(kv.Key))
                    {
                        kv.Value.Disabled = true;
                    }
                }
            }
            var token = $"{DateTime.Now:yyyy-MM-dd}_{GetUniqueKey(8)}";
            converter.ProcessJob(job, token).Wait();
            return 0;

        }
        private readonly Dictionary<ConfigYaml.TypeEnum, IHandler> handler;



        internal static X ReadYAML<X>(FileInfo fi) where X : class
        {
            if (!fi.Exists)
            {
                Console.WriteLine($"ERROR: file {fi.FullName} does not exist");
                return null;
            }
            var deserializer = new DeserializerBuilder().WithNamingConvention(new CamelCaseNamingConvention()).Build();

            try
            {
                var job = deserializer.Deserialize<X>(File.ReadAllText(fi.FullName));
                return job;
            }
            catch (YamlDotNet.Core.YamlException ex)
            {
                Console.WriteLine($"ERROR: Unable to read {fi.FullName}: {ex.Message} {ex.InnerException?.Message}");
                return null;
            }
        }
    }
}

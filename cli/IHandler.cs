﻿using System.Threading.Tasks;

namespace dbanonymizer
{
    interface IHandler
    {
        Task Anonymize(IOutput output, ConfigYaml config, JobYaml job, JobYaml._Anonymize anonymize);
    }
}

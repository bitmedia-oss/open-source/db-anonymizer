#!/bin/bash

IMAGE=registry.gitlab.com/bitmedia-oss/open-source/db-anonymizer

TAG=dev

if [ -n "$CI_REGISTRY_IMAGE" ]; then
  IMAGE=$CI_REGISTRY_IMAGE
fi
if [ -n "$CI_COMMIT_REF_SLUG" ]; then
  TAG=$CI_COMMIT_REF_SLUG
fi

if [ -n "$CI_COMMIT_TAG" ]; then
  TAG=wip-$CI_COMMIT_TAG
fi

echo Building $IMAGE:$TAG

docker build -t $IMAGE:$TAG .

if [ -n "$CI_REGISTRY_USER" ]; then
  docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY
  docker push $IMAGE:$TAG
fi

echo Done building $IMAGE:$TAG

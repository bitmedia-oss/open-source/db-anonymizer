FROM mcr.microsoft.com/dotnet/core/aspnet:3.1 AS base
WORKDIR /app

FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build
WORKDIR /src
COPY ["cli/cli.csproj", "cli/"]
COPY ./cli ./cli/
WORKDIR "/src/cli"
RUN dotnet publish "cli.csproj" -c Release -o /app

FROM base AS final

WORKDIR /app
COPY --from=build /app .
RUN apt-get update &&\
    apt-get install -y apt-transport-https gpg

RUN curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add - &&\
    touch /etc/apt/sources.list.d/kubernetes.list &&\
    echo "deb http://apt.kubernetes.io/ kubernetes-stretch main" |  tee -a /etc/apt/sources.list.d/kubernetes.list
RUN apt-get update &&\
    apt-get install -y kubectl p7zip git nano
ENV ANON_DATA_DIR=/app-data
VOLUME [ "/app-data" ]
ENTRYPOINT ["bash"]